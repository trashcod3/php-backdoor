A web shell (classified as a remote access trojan[1]) is a web security threat that is a web-based implementation of the shell concept.[2] A web shell is able to be uploaded to a web server to allow remote access to the web server, such as the web server's file system.[3] A web shell is unique in that it enables users to access a web server by way of a web browser that acts like a command-line interface.[4][5]
A user can access a remote computer via the World Wide Web using a web browser on any type of system, whether it's a desktop computer or a mobile phone with a web browser, and perform tasks on the remote system. No command-line environment is required on either the host or the client.[4][5]

A web shell could be programmed in any language that the target server supports. Web shells are most commonly written in PHP, Active Server Pages, or ASP.NET, but Python, Perl, Ruby and Unix shell scripts are also used, although not as common because it is not very common for web servers to support these languages.[3][4][5]

Using network monitoring tools such as Wireshark, an attacker can find vulnerabilities which are exploited resulting in a web shell installation. These vulnerabilities may be present in content management system applications (abbreviated CMS) or the web server's software.[4]

An attacker can use a web shell to issue commands, perform privilege escalation on the web server, and the ability to upload, delete, download and run scripts and files on the web server.[4] 

Source : https://en.wikipedia.org/wiki/Web_shell